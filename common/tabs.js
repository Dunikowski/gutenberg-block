window.onload = () => {

  const tab_elements = document.querySelectorAll('.wp-block-cs-gutenberg-blocks-tabs');

  tab_elements.forEach(function(e){

  const that = e;
  const colection_nav_items = that.querySelectorAll('.tab-item');
  const colection_items = that.querySelectorAll('.wp-block-cs-gutenberg-blocks-tabs-item');
  that.classList.add('init-cumulus-tab');
  
  const clear = () => {
  colection_nav_items.forEach((item) => {
    item.classList.remove('active');
  });

  colection_items.forEach((item) => {
    item.classList.remove('active');
    })
  }

  colection_items.forEach((item, index) => {
    if(index === 0) {
      item.classList.add('active');
    }
  })

  colection_nav_items.forEach(function(e) {
    e.addEventListener('click', (e) => {
      e.path.every(function(item) {
        if(item.classList && item.classList.contains('tab-item')) {
          e.stopPropagation();
          const target = item.attributes['data-c-item']['textContent'];
          const i = that.querySelector(`#c_${target}`);
          if(i) {
            clear();
            item.classList.add('active');
            i.classList.add('active');
          }
          return false;
        }

        return true;
      });

      });
    })
  });
};


