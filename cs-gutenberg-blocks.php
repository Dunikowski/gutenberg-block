<?php
/**
 * Plugin Name:       Cumulus Gutenberg Blocks
 * Description:       Gutenberg's block pack.
 * Requires at least: 5.7
 * Requires PHP:      7.0
 * Version:           0.1.0
 * Author:            Agencja Cumulus
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       create-block
 *
 */

function load_scripts_for_front_page() {
	$style_css = 'build/style-index.css';
	wp_register_style(
		'cs-gutenberg-blocks-css',
		plugins_url( $style_css, __FILE__ ),
		array()
	);

	wp_enqueue_style( 'cs-gutenberg-blocks-css' );

	$index_js ='common/tabs.js';
	
	wp_register_script(
		'cs-gutenberg-blocks-for-front-js',
		plugins_url( $index_js, __FILE__ ),
		array()
	);

	wp_enqueue_script('cs-gutenberg-blocks-for-front-js');
}

add_action('wp_enqueue_scripts', 'load_scripts_for_front_page'); 

function load_scripts_for_admin() {

	$script_asset_path = plugin_dir_path( __FILE__ ) . 'build/index.asset.php';
	$index_js     = 'build/index.js';
	$script_asset = require( $script_asset_path );

	wp_register_script(
		'cs-gutenberg-blocks-js',
		plugins_url( $index_js, __FILE__ ),
		$script_asset['dependencies'],
		$script_asset['version']
	);
	
	$editor_css = 'build/index.css';
	wp_register_style(
		'cs-gutenberg-blocks-editor-css',
		plugins_url( $editor_css, __FILE__ ),
		array()
	);

	wp_enqueue_style( 'cs-gutenberg-blocks-editor-css' );
	wp_enqueue_script( 'cs-gutenberg-blocks-js' );
}

add_action('admin_enqueue_scripts', 'load_scripts_for_admin');

function csGutenbergBlocksCategories( $categories, $post ) {
return array_merge(
	array(
		array(
			'slug' => 'cumulus',
			'title' => __( 'Cumulus','cumulus-block'),
		),
	),
	$categories
);
}
add_filter( 'block_categories', 'csGutenbergBlocksCategories', 10, 2);

 require_once plugin_dir_path( __FILE__ ).'src/Blocks/display-posts/display-posts.php';
