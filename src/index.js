/**
 * Registers a new block provided a unique name and an object defining its behavior.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-registration/
 */

import './style.scss';
import './editor.scss';
import "./Blocks/iterator-items/index";
import "./Blocks/iterator/index";
import "./Blocks/display-posts/index";
import "./Blocks/tabs/index";
import "./Blocks/tabs-items/index";
import "./Blocks/member-team/index";




