import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import { useBlockProps, InnerBlocks } from '@wordpress/block-editor';
import icons from "../icons/icons";

const {
	RichText, 
	InspectorControls
} = wp.blockEditor;

const {
	PanelBody, 
	CheckboxControl, 
	RadioControl
} = wp.components;

export default registerBlockType( 'cs-gutenberg-blocks/member-team', {
	apiVersion: 2,
	title: __( 'Member Team', 'cumulus-block' ),
	icon: icons.community,
	category: "cumulus",
	description: __(
		'Dla jak najbardziej realistycznego wyglądu bloku, należy dodawać zdjęcia w proporcji 4/3.','cumulus-block'
	),
	supports: {
		html: false,
	},
	attributes: {
		firstText: {
			type: "string",
			default: ""
		},
		secondText: {
			type: "string",
			default: ""
		},
		showSecondText: {
			type: "bool",
			default: true
		},
		postLayout: {
			type: "string",
			default: "main"
		}
	},
	edit: (props) => {
		const {attributes,setAttributes}=props;
		const ALLOWED_BLOCKS = [ 'core/image'];

		return ([
			<InspectorControls key="inspector-controls-for-members-team">
				<PanelBody 
					title={__('Ustawienia:')}
					initialOpen={true}
				>
			<RadioControl 
				label={__('Style')}
				selected={ attributes.postLayout}
				options={ [
						{ label: __('Blue'), value: 'main' },
						{ label: __('Red'), value: 'second' },
						{ label: __('Tryb autora'), value: 'author' },
				] }
				onChange={ ( data ) => setAttributes( {postLayout: data} )}
			/>
			<CheckboxControl
				label={__('Pokaż drugi obszar tekstowy')}
				checked={ attributes.showSecondText}
				onChange={ (newValue) => {setAttributes({showSecondText: newValue})}}
			/>
			
			</PanelBody>
			</InspectorControls>,
		<div { ...useBlockProps({className: `wp-block-cs-gutenberg-blocks-member-team--${attributes.postLayout}`}) } key="edit-content-for-members-team">

			<InnerBlocks key="main_innerblock"
			allowedBlocks={ ALLOWED_BLOCKS }
			/>
			<RichText key="editable_1"
					tagName="div"
					className="wp-block-cs-gutenberg-blocks-member-team__text-1"
					placeholder={__("Pierwszy obszar tekstowy")}
					multiline={true}
					value={attributes.firstText}
					onChange={(text) => setAttributes({ firstText: text })}
				/>
			{attributes.showSecondText && <RichText key="editable_2"
				tagName="div"
				className="wp-block-cs-gutenberg-blocks-member-team__text-2"
				placeholder={__("Drugi obszar tekstowy")}
				multiline={true}
				value={attributes.secondText}
				onChange={(text) => setAttributes({ secondText: text })}
			/>}
		</div>
		]
		);
	},

	save: ({attributes}) => {
	const i =	'wp-block-cs-gutenberg-blocks-member-team--'+ attributes.postLayout;
		const blockProps = useBlockProps.save( {
      className: i
    } );
		return (
			<div { ...blockProps}>
			<InnerBlocks.Content />
			<RichText.Content key="editable_1"
				tagName="div"
				className="wp-block-cs-gutenberg-blocks-member-team__text-1"
				value={attributes.firstText}
			/>
				{attributes.showSecondText && <RichText.Content key="editable_2"
				tagName="div"
				className="wp-block-cs-gutenberg-blocks-member-team__text-2"
				value={attributes.secondText}
			/>}
			</div>
		);
	},
} );
