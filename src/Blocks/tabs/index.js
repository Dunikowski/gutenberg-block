import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import { useBlockProps, InnerBlocks } from '@wordpress/block-editor';
const { useEffect} = wp.element;
const {select, withSelect} = wp.data;

import icons from "../icons/icons";

export default registerBlockType( 'cs-gutenberg-blocks/tabs', {
	apiVersion: 2,
	title: __( 'Blok tabs'),
	description: __(
		'Blok tabs','cumulus-block'
	),
	category: 'cumulus',
	icon: icons.tabs,
	supports: {
		html: false,
	},
	 attributes: {
	 	content: {
	 		'type' : 'string',
	 		'default' : ''
	 	}
	 },
	edit: withSelect( ( select, props) => {
		const { clientId } = props;
			
		return {
			c_block: select( 'core/block-editor' ).getBlock(clientId)
		};
	} )((props) => {
		const {attributes, c_block,setAttributes}=props;
		
		useEffect(() => {
			const t = c_block.innerBlocks.map(item => {
				return {content: item.attributes.content, id: item.attributes.item_number};
			});

			const T_stringfy = JSON.stringify(t);
			if(T_stringfy  !== attributes.content) {
				setAttributes({content: T_stringfy});
			}
	
		});
	
		const ALLOWED_BLOCKS = [ 'cs-gutenberg-blocks/tabs-item'];	

		return (
			<div { ...useBlockProps() }>
				<h2 className="wp-block-cs-gutenberg-blocks-tabs__heading">{__('Blok tabs')}</h2>
				<InnerBlocks 
				allowedBlocks={ ALLOWED_BLOCKS }
				/>
			</div>
		);
	}),
	save: ({attributes}) => {
		
		const nav = JSON.parse(attributes.content).map((item,index) => {
		 	return <li key={index}><span className={index == 0 ? 'active tab-item' : 'tab-item'} data-c-item={item.id}><span className="cs-gutenberg-blocks-tabs__icon"></span>{item.content}</span></li>
		})
	
		return (
			<div { ...useBlockProps.save() }>
				{nav &&  <ul className="nav-tab">{nav}</ul>}
					<div className="content-tab">
						<InnerBlocks.Content />
					</div>
			</div>
		);
	},
} );



