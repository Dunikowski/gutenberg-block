
import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import { useBlockProps, InnerBlocks, InspectorControls } from '@wordpress/block-editor';
import { CheckboxControl, PanelBody } from '@wordpress/components';
import icons from "../icons/icons";

export default registerBlockType( 'cs-gutenberg-blocks/iterator-items', {

	apiVersion: 2,
	title: __( 'Block item', 'cumulus-block' ),
	icon: icons.tab_item,
	parent: [ 'cs-gutenberg-blocks/iterator' ],
	description: __(
		'A single item within a repeat block.','cumulus-block'
	),
	supports: {
		html: false,
	},
	attributes:{
		displayitem:{
			type: 'boolean',
			default: true
		}
	},
	edit: (props) => {
		const { attributes, setAttributes, clientId} = props;

		const ALLOWED_BLOCKS  = wp.blocks.getBlockTypes()
								.filter((block) => { return block.name.indexOf('cs-gutenberg-blocks/iterator') === -1})
								.map((block) => { return block.name});		
		return ([
			<InspectorControls key="inspector-controls-for-iterator-items">
				<PanelBody
					title={__('Wyświetl element','cumulus-block')}
					initialOpen={true}
				>
				<CheckboxControl
					label={__('Element aktywny','cumulus-block')}
					checked={ attributes.displayitem}
					onChange={ (newValue) => {setAttributes({displayitem: newValue})}}
				/>
				</PanelBody>	
			</InspectorControls>,
			<div key={clientId} { ...useBlockProps() }>
				<InnerBlocks 
				allowedBlocks={ ALLOWED_BLOCKS }
				/>
			</div>
		]);
	},
	save: (props) => {	
		const {attributes} = props;
		return (
			attributes.displayitem && <div {...useBlockProps.save()}>
				<InnerBlocks.Content />
			</div>
		);
	},
} );
