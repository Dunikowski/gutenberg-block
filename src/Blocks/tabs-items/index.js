import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import { useBlockProps, InnerBlocks, RichText } from '@wordpress/block-editor';
import icons from "../icons/icons";

export default registerBlockType( 'cs-gutenberg-blocks/tabs-item', {
	apiVersion: 2,
	title: __( 'Block tabs item', 'cumulus-block' ),
	category: "cumulus",
	icon: icons.tab_item,
	parent: [ 'cs-gutenberg-blocks/tabs' ],
	description: __(
		'A single item within a repeat block.','cumulus-block'
	),
	supports: {
		html: false,
	},
	attributes:{
		content: {
			type: 'string',
			default: ''
		},
		item_number: {
			type: 'string',
			default: ''
		}
	},
	
	edit: (props) => {
			const {attributes,setAttributes, clientId}=props;
			const ALLOWED_BLOCKS  = wp.blocks.getBlockTypes()
									.filter((block) => { return block.name.indexOf('cs-gutenberg-blocks/tabs') === -1})
									.map((block) => { return block.name});
									setAttributes({item_number: clientId});
			return (
				<div { ...useBlockProps() }>
						<div className="wp-block-cs-gutenberg-blocks-tabs__heading">
						<RichText 
						value={ attributes.content } 
						onChange={ ( values ) => {
							setAttributes( {content:   values } );
						} }
						placeholder={ __( 'Heading...' ) }
						/>
					</div>
					<InnerBlocks 
					allowedBlocks={ ALLOWED_BLOCKS }
					/>
				</div>
			);
		},
	save: ({attributes}) => {

		return (
			<div {...useBlockProps.save({id: `c_${attributes.item_number}`})}>	
				<InnerBlocks.Content />
			</div>
		);
	},
} );


