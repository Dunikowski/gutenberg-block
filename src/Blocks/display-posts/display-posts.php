<?php

 function render_cs_gutenberg_blocks_display_posts($attributes, $content) {
	$args = array(
		'post_type' 	   => 'post',
		'category'		   => $attributes['displayPostByCatergories'] 
		? !empty($attributes['selectedCategorys']) 
		? $attributes['selectedCategorys']
		:[]
		:[],
		'posts_per_page'   => $attributes['postsToShow'],
		'offset'		   => $attributes['postsOffSet'],
		'post_status'      => 'publish',
		'order'            => $attributes['order'],
		'orderby'          => $attributes['orderBy'],
	);

	$posts = get_posts( $args );
	
	$list_items_markup = '';

	$display_color = isset($attributes['displayPostColor'])?$attributes['displayPostColor']: '#fff';

	$clock = '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
				<g id="Group_358" data-name="Group 358" transform="translate(-3175.094 3401.125)">
				<path id="Path_580" data-name="Path 580" d="M3185.094-3398.5a7.381,7.381,0,0,1,7.373,7.373,7.381,7.381,0,0,1-7.373,7.372,7.38,7.38,0,0,1-7.372-7.372,7.381,7.381,0,0,1,7.372-7.373m0-2.627a10,10,0,0,0-10,10,10,10,0,0,0,10,10,10,10,0,0,0,10-10,10,10,0,0,0-10-10Z" fill="'.$display_color.'" />
				<path id="Path_581" data-name="Path 581" d="M3185.921-3396.107h-1.957v5.939h4.295v-1.957h-2.338v-3.982Z" fill="'.$display_color.'" c/>
				</g>
			</svg>';

	
	$list_items_wrap = '';
	
	foreach ( $posts as $post ) {

		$list_items_content = '';
		$post_link = esc_url( get_permalink( $post ) );

		if (isset( $attributes['displayImage'] ) &&  $attributes['displayImage'] && has_post_thumbnail( $post ) ) {
			
			$featured_image = get_the_post_thumbnail(
				$post,
				$attributes['featuredImageSize']
			);
			$img_container_class = 'wp-block-cs-gutenberg-blocks-display-posts__img-container wp-block-cs-gutenberg-blocks-display-posts__link';
			
			if ( isset($attributes['addLinkToFeaturedImage']) && $attributes['addLinkToFeaturedImage'] && isset($attributes['postLayout']) &&  $attributes['postLayout'] === 'grid') {

				$list_items_content .= sprintf(
					'<a href="%1$s" class="%3$s"><figure>%2$s</figure></a>',
					$post_link,
					$featured_image,
					$img_container_class
				);
			} else {
				
				$list_items_content .= sprintf(
					'<div class="%1$s"><figure>%2$s</figure></div>',
					$img_container_class,
					$featured_image,
				);
			}
		}

		$list_items_content .= '<div class="wp-block-cs-gutenberg-blocks-display-posts__content">';

		if(isset($post->ID) && isset($attributes['displayCategorys']) && $attributes['displayCategorys']) {
			$categories = get_the_category($post->ID);

			if(! empty( $categories )) {

				$categories_class = 'wp-block-cs-gutenberg-blocks-display-posts__categorys-name';
				$list_categories = '';

				foreach ( $categories as $category ) {

					$cat_name = $category->cat_name;
					$cat_link = get_category_link($category);

					if(trim(strtolower($cat_name)) !== 'bez kategorii') {
						$list_categories .= sprintf(
							'<a href="%2$s">%1$s</a>',
							$cat_name,
							$cat_link
						);
					}
				}

				if(strlen($list_categories) > 1) {
					$list_items_content .= sprintf(
						'<div class="%1$s">%2$s</div>',
						$categories_class,
						$list_categories
					);
				}
			}
		}

		if(isset( $post->post_title ) ){

			$title = $post->post_title;
			$title_class = 'wp-block-cs-gutenberg-blocks-display-posts__heading';

			$list_items_content .= sprintf(
				'<a href="%1$s" class="%3$s">%2$s</a>',
				$post_link,
				$title,
				$title_class
			);
		}

		if ( isset( $attributes['displayPostDate'] ) && $attributes['displayPostDate'] ) {
			
			$date_pl =  get_the_date( '', $post );

			$list_items_content .= sprintf(
				'<time datetime="%1$s" class="wp-block-cs-gutenberg-blocks-display-posts__date">%3$s %2$s</time>',
				esc_attr( get_the_date( 'c', $post ) ),
				$date_pl,
				$clock
			);
		}

		$list_items_content .= '</div>';

		$main_class ='wp-block-cs-gutenberg-blocks-display-posts__item';
		

		if ( isset( $attributes['displayPostColor'] ) && $attributes['displayPostColor'] && isset( $attributes['displayBgPostColor'] ) && $attributes['displayBgPostColor'] ) {
			$list_items_wrap .= sprintf(
				'<div class="%1$s" style="color: %3$s; background-color: %4$s">%2$s</div>',
				$main_class,
				$list_items_content,
				$attributes['displayPostColor'],
				$attributes['displayBgPostColor']
			);
		} else {
			$list_items_wrap .= sprintf(
				'<div class="%1$s">%2$s</div>',
				$main_class,
				$list_items_content
			);
		}
	}

	$wrap_class = '';

	if(isset($attributes['postLayout']) && $attributes['postLayout'] !== 'list') {
		$wrap_class.= 'wp-block-cs-gutenberg-blocks-display-posts--'.$attributes['postLayout'];
	} else {
		$wrap_class.= 'wp-block-cs-gutenberg-blocks-display-posts';
	}

	if(isset($attributes['className']) && $attributes['className']) {
		$wrap_class.= ' '.$attributes['className'];
	}

	$list_items_markup .= sprintf(
		'<div class="%1$s">%2$s</div>',
		$wrap_class,
		$list_items_wrap,
	);

	return $list_items_markup;
 }

/**
 * Registering dynamic block.
 */

function cs_gutenberg_blocks_display_posts() {

	if ( function_exists( 'register_block_type' ) ) {
		register_block_type_from_metadata( __DIR__,array(
			'render_callback' => 'render_cs_gutenberg_blocks_display_posts',	
		) );
	}
}

add_action( 'init', 'cs_gutenberg_blocks_display_posts' );
