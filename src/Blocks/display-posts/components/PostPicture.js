const { 
    withSelect, 
    select 
} = wp.data;

const { 
    Fragment 
} = wp.element;

function PostPicture(props) {
   
    const {
        media, 
        size, 
        post_link 
    } = props;
  
    let source = null;

    if(media) {
        if(media.media_details.sizes[size]) {
            source = {};
            source['url'] = media.media_details.sizes[size].source_url;
        } else {
            source = null;
        }
    }
   let isLink = false;

   if(post_link !== '') {
        isLink = true;
   }

    return (
        <Fragment>
            { source && (
                    <div className="wp-block-cs-gutenberg-blocks-display-posts__img-container">
                        <img className="wp-block-cs-gutenberg-blocks-display-posts__img" src={source['url']} alt={media.alt_text}/>
                    </div>
                )
            }
        </Fragment>  
    );
}

export default  withSelect( (select, props ) => {
    let _media = [],
        _size = '',
        _post_link = '';
    

    if(typeof props.id === 'number' ) {
        _media = select('core').getMedia(props.id);
    } 

    if(typeof props.attributes.featuredImageSize === 'string' ) {
        _size = props.attributes.featuredImageSize;
    }

    if(typeof props.post_link === 'string' && props.attributes.addLinkToFeaturedImage) {
        _post_link = props.post_link;
    }

 return {
        media: _media,
        size: _size,
        post_link: _post_link
    };

  })(PostPicture);