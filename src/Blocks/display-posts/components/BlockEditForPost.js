
import { useBlockProps } from '@wordpress/block-editor';
import { __ } from '@wordpress/i18n';
import PostPicture from './PostPicture';
import CategorysNameForPost from './CategorysNameForPost';
import icons from '../../icons/icons';
const { withSelect, select } = wp.data;
const { Spinner } = wp.components;


const filterObjectAttributes = (obj, condition) => Object.fromEntries(Object.entries(obj).filter(condition));

export default withSelect( (select, props ) => {

    const {
        order,
        orderBy,
        postsToShow,
        postsOffSet,
        selectedCategorys,
        displayPostByCatergories
    } = props.attributes	

    const { getCurrentPostId } = select("core/editor");
    const getPosts = filterObjectAttributes(
        {
            categories: displayPostByCatergories
                ? !selectedCategorys.includes('brak') && selectedCategorys.length > 0
                ? selectedCategorys 
                :[]
                :[],
            orderby: orderBy,
            order: order,
            per_page: postsToShow,
            offset: postsOffSet,
            exclude: [getCurrentPostId()],
        },
        (value) => typeof value !== "undefined"
    );

    return {
        posts: wp.data.select('core').getEntityRecords('postType', 'post',getPosts)
    };

  })((props) => {

    const { attributes, posts} = props;
    const { 
        displayCategorys,
        displayPostDate ,
        postLayout,
        displayImage,
        displayPostColor,
        displayBgPostColor
    } = attributes;

    const _isArray = Array.isArray(posts);

    if(!_isArray) {
        return (
            <div {...useBlockProps()}>
                <Spinner />
            </div>
        )
    }
    
    if(_isArray && posts.length === 0 ) {
        return (
        <div {...useBlockProps()}>
            <div className="wp-block-cs-gutenberg-blocks-display-posts__missing-posts">Brak wpisów</div>
        </div>  
        )
    }
   
    if(_isArray && posts.length > 0) {

        const results = posts.map((post, index) => 
        <div key={index} style={{color: displayPostColor, backgroundColor: displayBgPostColor}} className={'wp-block-cs-gutenberg-blocks-display-posts__item'}>
          {displayImage && (<PostPicture 
                id={post.featured_media} 
                attributes={attributes}
                post_link={post.guid.rendered}
            />)
            }
          <div className="wp-block-cs-gutenberg-blocks-display-posts__content">
            {displayCategorys && <CategorysNameForPost post={post}/>} 
            <span className="wp-block-cs-gutenberg-blocks-display-posts__heading">{post.title.raw}</span>
            {displayPostDate && (<div className="wp-block-cs-gutenberg-blocks-display-posts__date">
                {icons.clock}<span>{post.date.substr(0,post.date.indexOf("T")).replaceAll('-',' ')}</span>
            </div>)}
          </div>
        </div>);

        return (
            <div {...useBlockProps({
                className: (postLayout !== 'list')? `wp-block-cs-gutenberg-blocks-display-posts--${postLayout}`:''
            })}>
                {results}
            </div>
        );
    }
  })