const { withSelect, select } = wp.data;
const { Fragment} = wp.element;

import { __ } from '@wordpress/i18n';

function CategorysNameForPost(props) {

   const { categorys, post } = props;
   const emptyCategorys = Array.isArray(categorys);

  if(!emptyCategorys) {
      return (
          <span>{__('Ładowanie kategorii')} ...</span>  
      );
  }

  let categoryName = [];
  
  if(post && emptyCategorys ) {
       categorys.forEach( category => {
           if(Array.isArray(post.categories) && post.categories.includes(category.id)) {
            categoryName.push(category.name);
           }
       }); 
  }

  if(emptyCategorys && (categoryName.length  === 0)){
      return '';
  }

  if(categoryName.length > 0) {

    const categorys_element = categoryName
    .map(item => 
      <li key={item}>{item}</li>
    )
  
    return categorys_element.length > 0 ? (
      <ul className="wp-block-cs-gutenberg-blocks-display-posts__categorys-name">
        {categorys_element}
      </ul>  
  ) : '';
  }
}
export default  withSelect( (select, props ) => {
 

 return {
        categorys: wp.data.select('core').getEntityRecords('taxonomy', 'category',{per_page: -1}),
        post: props.post
    };

  })(CategorysNameForPost)