const { withSelect, select } = wp.data;
const {useState, Fragment} = wp.element;

import { __ } from '@wordpress/i18n';

function PostAuthor(props) {

   const { authors, post } = props;
   const emptyAuthors = Array.isArray(authors);

  if(!emptyAuthors) {
      return (
          <span>Loading athors ...</span>  
      );
  }
let authorName = '';
  if(post && emptyAuthors ) {
      authors.forEach( author => {
          if(author.id === post.author) {
            authorName = author.name;
            return;
          }
      }); 
  }

  if(emptyAuthors && (authorName === '')){
      return (
          <span>Nie znaleziono autora wpisu</span>  
      );
  }

  if(authorName !== '') {
    return (
      <div>
        <span>{__('Autor')}:</span>
        <span>{authorName}</span>
      </div>  
  );
  }
}
export default  withSelect( (select, props ) => {
 

 return {
        authors: wp.data.select('core').getAuthors(),
        post: props.post
    };

  })(PostAuthor)