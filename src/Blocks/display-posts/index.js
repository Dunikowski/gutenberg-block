import { __ } from '@wordpress/i18n';
import { registerBlockType } from '@wordpress/blocks';
import { InspectorControls } from '@wordpress/block-editor';
import BlockEditForPost from "./components/BlockEditForPost";
import icons from "../icons/icons";

const { 
	SelectControl, 
	PanelBody, 
	CheckboxControl, 
	RadioControl, 
	RangeControl,
	ColorPicker
} = wp.components;

const { 
	withSelect, 
	select 
} = wp.data;

registerBlockType( 'cs-gutenberg-blocks/display-posts', {
	icon: icons.post,
	edit: withSelect( (select, props ) => {
 
		return {
					 categorys: wp.data.select('core').getEntityRecords('taxonomy', 'category',{per_page: -1}),
			 };
	 
		 })(({attributes,categorys, setAttributes}) => {
	
			fetch('/wp-json/wp/v2/posts',{method: 'HEAD'}).then(res => {
	
				 const total = parseInt(res.headers.get('x-wp-total'));
	
				 if(total <= 15){
					setAttributes({maxPostsOffSet: total});
				 } else {
					setAttributes({maxPostsOffSet: 15});
				 }
			
			});
	
			let category_option = [];
			if(categorys) {
				category_option  = categorys.map((item) => {
					return { value: item.id, label: __(item.name)}
				});
				
				category_option.unshift({ value:'brak', label: __('Brak') });
			}
		
		return ([
			<InspectorControls key="inspector-controls-for-display-posts">
				<PanelBody
					title={__('Sortowanie i filtrowanie')}
					initialOpen={true}
				>
					<SelectControl
						label={__('Sortowanie')}
						value={ attributes.order}
						options={ [
								{ label: __('Rosnące'), value: 'asc' },
								{ label: __('Malejące'), value: 'desc' },
						] }
						onChange={ (data) => setAttributes( {order: data} )}
				/>
				<SelectControl
					label={__('Sortowanie po?')}
					value={ attributes.orderBy}
					options={ [
						{ label: __('Dacie'), value: 'date' },
						{ label: __('Tytule'), value: 'title' },
					] }
					onChange={ (data) => setAttributes( {orderBy: data} )}
				/>
				<CheckboxControl
					label={__('Wybierz kategorie postów do wyświetlenia')}
					checked={ attributes.displayPostByCatergories}
					onChange={ (value) => setAttributes( {displayPostByCatergories: value} )}
				/>
				{attributes.displayPostByCatergories &&	(<SelectControl
						multiple
						label={ __( 'Sortowanie po kategorii:' ) }
						value={ attributes.selectedCategorys } 
						onChange={ ( values) => {
							setAttributes( { selectedCategorys: values} );
						} }
						options={ category_option }
				/>)}
				<RangeControl 
					label={__('Liczba postów')}
					value={ attributes.postsToShow}
					onChange={ (data) => setAttributes( {postsToShow: data} )}
					min={ 1 }
					max={ 20 }
				/>
				<RangeControl 
					label={__('Pokaż posty od')}
					value={ attributes.postsOffSet}
					onChange={ (data) => {
	
						setAttributes( {postsOffSet: data} );
					}}
					min={ 1 }
					max={ attributes.maxPostsOffSet }
				/>
			</PanelBody>
			<PanelBody
					title={__('Ustawienie posta','cumulus-block')}
					initialOpen={true}
				>
				<RadioControl 
					label={__('Layout','cumulus-block')}
					selected={ attributes.postLayout}
					options={ [
							{ label: __('Lista'), value: 'list' },
							{ label: __('Grid'), value: 'grid' },
							{ label: __('Kolumna'), value: 'column' },
					] }
					onChange={ ( data ) => setAttributes( {postLayout: data} )}
				/>
				<CheckboxControl
					label={__('Pokaż kategorie')}
					checked={ attributes.displayCategorys}
					onChange={ (data) => setAttributes( {displayCategorys: data} )}
				/>
				<CheckboxControl
					label={__('Pokaż date')}
					checked={ attributes.displayPostDate}
					onChange={ (data) => setAttributes( {displayPostDate: data} )}
				/>
			</PanelBody>
			<PanelBody
				title={__('Kolory')}
				initialOpen={false}
			>
					<p>{__('Kolor tekstu')}</p>
				<ColorPicker
					color={  attributes.displayPostColor }
					onChangeComplete={ ( value ) =>  {
						setAttributes( {displayPostColor : value.hex})
					} }
					disableAlpha
					/>
					<p>{__('Kolor tła')}</p>
					<ColorPicker
					color={  attributes.displayBgPostColor}
					onChangeComplete={ ( value ) => setAttributes( {displayBgPostColor: value.hex }) }
					disableAlpha
					/>
			</PanelBody>
			<PanelBody
					title={__('Ustawienia obrazka')}
					initialOpen={true}
				>
				<CheckboxControl
					label={__('Pokaż zdjęcie postu')}
					checked={ attributes.displayImage}
					onChange={ (data) => setAttributes( {displayImage: data} )}
				/>
				<SelectControl
						label={__('Wybierz rozmiar obrazka')}
						value={ attributes.featuredImageSize}
						options={ [
								{ label: __('Ikona'), value: 'thumbnail' },
								{ label: __('Średni'), value: 'medium' },
								{ label: __('Średni większy'), value: 'medium_large' },
								{ label: __('Duży'), value: 'large' },
								{ label: __('Full'), value: 'full' },
						] }
						onChange={ (size) => setAttributes( {featuredImageSize: size} )}
				/>
				<CheckboxControl
					label={__('Dodaj link do zdjęcia. (Działa tylko, gdy layout ustawiony jest na grid)')}
					checked={ attributes.addLinkToFeaturedImage}
					onChange={ (data) => setAttributes( {addLinkToFeaturedImage: data} )}
				/>
				</PanelBody>
			</InspectorControls>,
			<BlockEditForPost attributes={attributes} key="block-edit-for-post"/>
			]
		);
	}),
	save: () => {return null},
} );
