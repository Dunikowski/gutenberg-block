import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import { useBlockProps, InnerBlocks } from '@wordpress/block-editor';
import icons from "../icons/icons";

export default registerBlockType( 'cs-gutenberg-blocks/iterator', {
	apiVersion: 2,
	category: "cumulus",
	icon: icons.repeat,
	textdomain: "cumulus-block",
	supports: {
		html: false
	},
	title: __(
		'Iterator','cumulus-block'
	),
	 description: __(
		'Blok iteratora','cumulus-block'
	),

	edit: () => {
		const ALLOWED_BLOCKS = [ 'cs-gutenberg-blocks/iterator-items'];
		return (
			<div { ...useBlockProps() } >
				<h2 className="wp-block-cs-gutenberg-blocks-iterator__heading">{__('Blok iteratora','cumulus-block')}</h2>
				<InnerBlocks 
				allowedBlocks={ ALLOWED_BLOCKS }
				/>
			</div>
		);
	},
	save: () => {
		
		return (
			<div { ...useBlockProps.save() }>
				<InnerBlocks.Content />
			</div>
		);
	},
} );

