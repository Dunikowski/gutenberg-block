(window["webpackJsonp_cs_gutenberg_blocks"] = window["webpackJsonp_cs_gutenberg_blocks"] || []).push([["style-index"],{

/***/ "./src/style.scss":
/*!************************!*\
  !*** ./src/style.scss ***!
  \************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

}]);

/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"index": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp_cs_gutenberg_blocks"] = window["webpackJsonp_cs_gutenberg_blocks"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","style-index"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/helpers/extends.js":
/*!********************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/extends.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _extends() {
  module.exports = _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  module.exports["default"] = module.exports, module.exports.__esModule = true;
  return _extends.apply(this, arguments);
}

module.exports = _extends;
module.exports["default"] = module.exports, module.exports.__esModule = true;

/***/ }),

/***/ "./src/Blocks/display-posts/components/BlockEditForPost.js":
/*!*****************************************************************!*\
  !*** ./src/Blocks/display-posts/components/BlockEditForPost.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _PostPicture__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./PostPicture */ "./src/Blocks/display-posts/components/PostPicture.js");
/* harmony import */ var _CategorysNameForPost__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./CategorysNameForPost */ "./src/Blocks/display-posts/components/CategorysNameForPost.js");
/* harmony import */ var _icons_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../icons/icons */ "./src/Blocks/icons/icons.js");






const {
  withSelect,
  select
} = wp.data;
const {
  Spinner
} = wp.components;

const filterObjectAttributes = (obj, condition) => Object.fromEntries(Object.entries(obj).filter(condition));

/* harmony default export */ __webpack_exports__["default"] = (withSelect((select, props) => {
  const {
    order,
    orderBy,
    postsToShow,
    postsOffSet,
    selectedCategorys,
    displayPostByCatergories
  } = props.attributes;
  const {
    getCurrentPostId
  } = select("core/editor");
  const getPosts = filterObjectAttributes({
    categories: displayPostByCatergories ? !selectedCategorys.includes('brak') && selectedCategorys.length > 0 ? selectedCategorys : [] : [],
    orderby: orderBy,
    order: order,
    per_page: postsToShow,
    offset: postsOffSet,
    exclude: [getCurrentPostId()]
  }, value => typeof value !== "undefined");
  return {
    posts: wp.data.select('core').getEntityRecords('postType', 'post', getPosts)
  };
})(props => {
  const {
    attributes,
    posts
  } = props;
  const {
    displayCategorys,
    displayPostDate,
    postLayout,
    displayImage,
    displayPostColor,
    displayBgPostColor
  } = attributes;

  const _isArray = Array.isArray(posts);

  if (!_isArray) {
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", Object(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_1__["useBlockProps"])(), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(Spinner, null));
  }

  if (_isArray && posts.length === 0) {
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", Object(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_1__["useBlockProps"])(), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      className: "wp-block-cs-gutenberg-blocks-display-posts__missing-posts"
    }, "Brak wpis\xF3w"));
  }

  if (_isArray && posts.length > 0) {
    const results = posts.map((post, index) => Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      key: index,
      style: {
        color: displayPostColor,
        backgroundColor: displayBgPostColor
      },
      className: 'wp-block-cs-gutenberg-blocks-display-posts__item'
    }, displayImage && Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_PostPicture__WEBPACK_IMPORTED_MODULE_3__["default"], {
      id: post.featured_media,
      attributes: attributes,
      post_link: post.guid.rendered
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      className: "wp-block-cs-gutenberg-blocks-display-posts__content"
    }, displayCategorys && Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_CategorysNameForPost__WEBPACK_IMPORTED_MODULE_4__["default"], {
      post: post
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("span", {
      className: "wp-block-cs-gutenberg-blocks-display-posts__heading"
    }, post.title.raw), displayPostDate && Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      className: "wp-block-cs-gutenberg-blocks-display-posts__date"
    }, _icons_icons__WEBPACK_IMPORTED_MODULE_5__["default"].clock, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("span", null, post.date.substr(0, post.date.indexOf("T")).replaceAll('-', ' '))))));
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", Object(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_1__["useBlockProps"])({
      className: postLayout !== 'list' ? `wp-block-cs-gutenberg-blocks-display-posts--${postLayout}` : ''
    }), results);
  }
}));

/***/ }),

/***/ "./src/Blocks/display-posts/components/CategorysNameForPost.js":
/*!*********************************************************************!*\
  !*** ./src/Blocks/display-posts/components/CategorysNameForPost.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__);

const {
  withSelect,
  select
} = wp.data;
const {
  Fragment
} = wp.element;


function CategorysNameForPost(props) {
  const {
    categorys,
    post
  } = props;
  const emptyCategorys = Array.isArray(categorys);

  if (!emptyCategorys) {
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("span", null, Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Ładowanie kategorii'), " ...");
  }

  let categoryName = [];

  if (post && emptyCategorys) {
    categorys.forEach(category => {
      if (Array.isArray(post.categories) && post.categories.includes(category.id)) {
        categoryName.push(category.name);
      }
    });
  }

  if (emptyCategorys && categoryName.length === 0) {
    return '';
  }

  if (categoryName.length > 0) {
    const categorys_element = categoryName.map(item => Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("li", {
      key: item
    }, item));
    return categorys_element.length > 0 ? Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("ul", {
      className: "wp-block-cs-gutenberg-blocks-display-posts__categorys-name"
    }, categorys_element) : '';
  }
}

/* harmony default export */ __webpack_exports__["default"] = (withSelect((select, props) => {
  return {
    categorys: wp.data.select('core').getEntityRecords('taxonomy', 'category', {
      per_page: -1
    }),
    post: props.post
  };
})(CategorysNameForPost));

/***/ }),

/***/ "./src/Blocks/display-posts/components/PostPicture.js":
/*!************************************************************!*\
  !*** ./src/Blocks/display-posts/components/PostPicture.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

const {
  withSelect,
  select
} = wp.data;
const {
  Fragment
} = wp.element;

function PostPicture(props) {
  const {
    media,
    size,
    post_link
  } = props;
  let source = null;

  if (media) {
    if (media.media_details.sizes[size]) {
      source = {};
      source['url'] = media.media_details.sizes[size].source_url;
    } else {
      source = null;
    }
  }

  let isLink = false;

  if (post_link !== '') {
    isLink = true;
  }

  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(Fragment, null, source && Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
    className: "wp-block-cs-gutenberg-blocks-display-posts__img-container"
  }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("img", {
    className: "wp-block-cs-gutenberg-blocks-display-posts__img",
    src: source['url'],
    alt: media.alt_text
  })));
}

/* harmony default export */ __webpack_exports__["default"] = (withSelect((select, props) => {
  let _media = [],
      _size = '',
      _post_link = '';

  if (typeof props.id === 'number') {
    _media = select('core').getMedia(props.id);
  }

  if (typeof props.attributes.featuredImageSize === 'string') {
    _size = props.attributes.featuredImageSize;
  }

  if (typeof props.post_link === 'string' && props.attributes.addLinkToFeaturedImage) {
    _post_link = props.post_link;
  }

  return {
    media: _media,
    size: _size,
    post_link: _post_link
  };
})(PostPicture));

/***/ }),

/***/ "./src/Blocks/display-posts/index.js":
/*!*******************************************!*\
  !*** ./src/Blocks/display-posts/index.js ***!
  \*******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/blocks */ "@wordpress/blocks");
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_BlockEditForPost__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/BlockEditForPost */ "./src/Blocks/display-posts/components/BlockEditForPost.js");
/* harmony import */ var _icons_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../icons/icons */ "./src/Blocks/icons/icons.js");






const {
  SelectControl,
  PanelBody,
  CheckboxControl,
  RadioControl,
  RangeControl,
  ColorPicker
} = wp.components;
const {
  withSelect,
  select
} = wp.data;
Object(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_2__["registerBlockType"])('cs-gutenberg-blocks/display-posts', {
  icon: _icons_icons__WEBPACK_IMPORTED_MODULE_5__["default"].post,
  edit: withSelect((select, props) => {
    return {
      categorys: wp.data.select('core').getEntityRecords('taxonomy', 'category', {
        per_page: -1
      })
    };
  })(({
    attributes,
    categorys,
    setAttributes
  }) => {
    fetch('/wp-json/wp/v2/posts', {
      method: 'HEAD'
    }).then(res => {
      const total = parseInt(res.headers.get('x-wp-total'));

      if (total <= 15) {
        setAttributes({
          maxPostsOffSet: total
        });
      } else {
        setAttributes({
          maxPostsOffSet: 15
        });
      }
    });
    let category_option = [];

    if (categorys) {
      category_option = categorys.map(item => {
        return {
          value: item.id,
          label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])(item.name)
        };
      });
      category_option.unshift({
        value: 'brak',
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Brak')
      });
    }

    return [Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__["InspectorControls"], {
      key: "inspector-controls-for-display-posts"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(PanelBody, {
      title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Sortowanie i filtrowanie'),
      initialOpen: true
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(SelectControl, {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Sortowanie'),
      value: attributes.order,
      options: [{
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Rosnące'),
        value: 'asc'
      }, {
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Malejące'),
        value: 'desc'
      }],
      onChange: data => setAttributes({
        order: data
      })
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(SelectControl, {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Sortowanie po?'),
      value: attributes.orderBy,
      options: [{
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Dacie'),
        value: 'date'
      }, {
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Tytule'),
        value: 'title'
      }],
      onChange: data => setAttributes({
        orderBy: data
      })
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(CheckboxControl, {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Wybierz kategorie postów do wyświetlenia'),
      checked: attributes.displayPostByCatergories,
      onChange: value => setAttributes({
        displayPostByCatergories: value
      })
    }), attributes.displayPostByCatergories && Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(SelectControl, {
      multiple: true,
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Sortowanie po kategorii:'),
      value: attributes.selectedCategorys,
      onChange: values => {
        setAttributes({
          selectedCategorys: values
        });
      },
      options: category_option
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(RangeControl, {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Liczba postów'),
      value: attributes.postsToShow,
      onChange: data => setAttributes({
        postsToShow: data
      }),
      min: 1,
      max: 20
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(RangeControl, {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Pokaż posty od'),
      value: attributes.postsOffSet,
      onChange: data => {
        setAttributes({
          postsOffSet: data
        });
      },
      min: 1,
      max: attributes.maxPostsOffSet
    })), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(PanelBody, {
      title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Ustawienie posta', 'cumulus-block'),
      initialOpen: true
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(RadioControl, {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Layout', 'cumulus-block'),
      selected: attributes.postLayout,
      options: [{
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Lista'),
        value: 'list'
      }, {
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Grid'),
        value: 'grid'
      }, {
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Kolumna'),
        value: 'column'
      }],
      onChange: data => setAttributes({
        postLayout: data
      })
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(CheckboxControl, {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Pokaż kategorie'),
      checked: attributes.displayCategorys,
      onChange: data => setAttributes({
        displayCategorys: data
      })
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(CheckboxControl, {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Pokaż date'),
      checked: attributes.displayPostDate,
      onChange: data => setAttributes({
        displayPostDate: data
      })
    })), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(PanelBody, {
      title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Kolory'),
      initialOpen: false
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("p", null, Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Kolor tekstu')), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(ColorPicker, {
      color: attributes.displayPostColor,
      onChangeComplete: value => {
        setAttributes({
          displayPostColor: value.hex
        });
      },
      disableAlpha: true
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("p", null, Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Kolor tła')), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(ColorPicker, {
      color: attributes.displayBgPostColor,
      onChangeComplete: value => setAttributes({
        displayBgPostColor: value.hex
      }),
      disableAlpha: true
    })), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(PanelBody, {
      title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Ustawienia obrazka'),
      initialOpen: true
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(CheckboxControl, {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Pokaż zdjęcie postu'),
      checked: attributes.displayImage,
      onChange: data => setAttributes({
        displayImage: data
      })
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(SelectControl, {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Wybierz rozmiar obrazka'),
      value: attributes.featuredImageSize,
      options: [{
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Ikona'),
        value: 'thumbnail'
      }, {
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Średni'),
        value: 'medium'
      }, {
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Średni większy'),
        value: 'medium_large'
      }, {
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Duży'),
        value: 'large'
      }, {
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Full'),
        value: 'full'
      }],
      onChange: size => setAttributes({
        featuredImageSize: size
      })
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(CheckboxControl, {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])('Dodaj link do zdjęcia. (Działa tylko, gdy layout ustawiony jest na grid)'),
      checked: attributes.addLinkToFeaturedImage,
      onChange: data => setAttributes({
        addLinkToFeaturedImage: data
      })
    }))), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_components_BlockEditForPost__WEBPACK_IMPORTED_MODULE_4__["default"], {
      attributes: attributes,
      key: "block-edit-for-post"
    })];
  }),
  save: () => {
    return null;
  }
});

/***/ }),

/***/ "./src/Blocks/icons/icons.js":
/*!***********************************!*\
  !*** ./src/Blocks/icons/icons.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

const clock = Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  width: "20",
  height: "20",
  viewBox: "0 0 20 20"
}, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("g", {
  id: "Group_358",
  "data-name": "Group 358",
  transform: "translate(-3175.094 3401.125)"
}, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  id: "Path_580",
  "data-name": "Path 580",
  d: "M3185.094-3398.5a7.381,7.381,0,0,1,7.373,7.373,7.381,7.381,0,0,1-7.373,7.372,7.38,7.38,0,0,1-7.372-7.372,7.381,7.381,0,0,1,7.372-7.373m0-2.627a10,10,0,0,0-10,10,10,10,0,0,0,10,10,10,10,0,0,0,10-10,10,10,0,0,0-10-10Z"
}), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  id: "Path_581",
  "data-name": "Path 581",
  d: "M3185.921-3396.107h-1.957v5.939h4.295v-1.957h-2.338v-3.982Z"
})));
const post = Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("svg", {
  id: "_x31__x2C_5_px",
  height: "512",
  viewBox: "0 0 24 24",
  width: "512",
  xmlns: "http://www.w3.org/2000/svg"
}, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "m17.5 24c-3.584 0-6.5-2.916-6.5-6.5s2.916-6.5 6.5-6.5 6.5 2.916 6.5 6.5-2.916 6.5-6.5 6.5zm0-11.5c-2.757 0-5 2.243-5 5s2.243 5 5 5 5-2.243 5-5-2.243-5-5-5z"
}), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "m17.5 21c-.414 0-.75-.336-.75-.75v-5.5c0-.414.336-.75.75-.75s.75.336.75.75v5.5c0 .414-.336.75-.75.75z"
}), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "m20.25 18.25h-5.5c-.414 0-.75-.336-.75-.75s.336-.75.75-.75h5.5c.414 0 .75.336.75.75s-.336.75-.75.75z"
}), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "m9.19 21h-6.44c-1.517 0-2.75-1.233-2.75-2.75v-15.5c0-1.517 1.233-2.75 2.75-2.75h11.5c1.517 0 2.75 1.233 2.75 2.75v6.09c0 .414-.336.75-.75.75s-.75-.336-.75-.75v-6.09c0-.689-.561-1.25-1.25-1.25h-11.5c-.689 0-1.25.561-1.25 1.25v15.5c0 .689.561 1.25 1.25 1.25h6.44c.414 0 .75.336.75.75s-.336.75-.75.75z"
}), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "m13.25 9.5h-9.5c-.414 0-.75-.336-.75-.75s.336-.75.75-.75h9.5c.414 0 .75.336.75.75s-.336.75-.75.75z"
}), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "m9.25 13.5h-5.5c-.414 0-.75-.336-.75-.75s.336-.75.75-.75h5.5c.414 0 .75.336.75.75s-.336.75-.75.75z"
}), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "m8.25 5.5h-4.5c-.414 0-.75-.336-.75-.75s.336-.75.75-.75h4.5c.414 0 .75.336.75.75s-.336.75-.75.75z"
}));
const repeat = Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  width: "24pt",
  height: "24pt",
  viewBox: "0 0 24 24",
  version: "1.1"
}, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("g", {
  id: "surface1"
}, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "M 23.929688 13.933594 C 23.804688 13.667969 23.539062 13.5 23.25 13.5 L 21 13.5 L 21 7.5 C 21 5.84375 19.652344 4.5 18 4.5 L 6.804688 4.5 L 9.304688 7.5 L 18 7.5 L 18 13.5 L 15.75 13.5 C 15.457031 13.5 15.195312 13.667969 15.070312 13.933594 C 14.949219 14.195312 14.988281 14.507812 15.175781 14.730469 L 18.921875 19.230469 C 19.066406 19.402344 19.277344 19.5 19.5 19.5 C 19.722656 19.5 19.933594 19.402344 20.074219 19.230469 L 23.824219 14.730469 C 24.011719 14.507812 24.050781 14.195312 23.929688 13.933594 Z M 23.929688 13.933594 "
}), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "M 14.695312 16.5 L 6 16.5 L 6 10.5 L 8.25 10.5 C 8.542969 10.5 8.804688 10.332031 8.929688 10.066406 C 9.050781 9.804688 9.011719 9.492188 8.824219 9.269531 L 5.074219 4.769531 C 4.933594 4.597656 4.722656 4.5 4.5 4.5 C 4.277344 4.5 4.066406 4.597656 3.925781 4.769531 L 0.175781 9.269531 C -0.0117188 9.492188 -0.0507812 9.804688 0.0703125 10.066406 C 0.195312 10.332031 0.460938 10.5 0.75 10.5 L 3 10.5 L 3 16.5 C 3 18.15625 4.34375 19.5 6 19.5 L 17.195312 19.5 Z M 14.695312 16.5 "
})));
const tabs = Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  width: "24pt",
  height: "24pt",
  viewBox: "0 0 24 24",
  version: "1.1"
}, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("g", null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "M 21.796875 4.753906 L 21.796875 3.75 C 21.796875 2.199219 20.535156 0.9375 18.984375 0.9375 L 2.8125 0.9375 C 1.261719 0.9375 0 2.199219 0 3.75 L 0 20.25 C 0 21.800781 1.261719 23.0625 2.8125 23.0625 L 21.1875 23.0625 C 22.738281 23.0625 24 21.800781 24 20.25 L 24 7.5 C 24 6.160156 23.054688 5.035156 21.796875 4.753906 Z M 19.921875 3.75 L 19.921875 4.6875 L 15.703125 4.6875 L 15.703125 3.75 C 15.703125 3.421875 15.644531 3.105469 15.542969 2.8125 L 18.984375 2.8125 C 19.5 2.8125 19.921875 3.234375 19.921875 3.75 Z M 12.890625 2.8125 C 13.40625 2.8125 13.828125 3.234375 13.828125 3.75 L 13.828125 4.6875 L 9.5625 4.6875 L 9.5625 3.75 C 9.5625 3.421875 9.503906 3.105469 9.402344 2.8125 Z M 22.125 20.25 C 22.125 20.765625 21.703125 21.1875 21.1875 21.1875 L 2.8125 21.1875 C 2.296875 21.1875 1.875 20.765625 1.875 20.25 L 1.875 3.75 C 1.875 3.234375 2.296875 2.8125 2.8125 2.8125 L 6.75 2.8125 C 7.265625 2.8125 7.6875 3.234375 7.6875 3.75 L 7.6875 6.5625 L 21.1875 6.5625 C 21.703125 6.5625 22.125 6.984375 22.125 7.5 Z M 22.125 20.25 "
})));
const tab_item = Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  width: "24pt",
  height: "24pt",
  viewBox: "0 0 24 24",
  version: "1.1"
}, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("g", null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "M 23.25 22 C 22.835938 22 22.5 21.664062 22.5 21.25 L 22.5 2.75 C 22.5 2.0625 21.9375 1.5 21.25 1.5 L 2.75 1.5 C 2.0625 1.5 1.5 2.0625 1.5 2.75 L 1.5 15.25 C 1.5 15.664062 1.164062 16 0.75 16 C 0.335938 16 0 15.664062 0 15.25 L 0 2.75 C 0 1.234375 1.234375 0 2.75 0 L 21.25 0 C 22.765625 0 24 1.234375 24 2.75 L 24 21.25 C 24 21.664062 23.664062 22 23.25 22 Z M 23.25 22 "
}), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "M 23.25 5 L 0.75 5 C 0.335938 5 0 4.664062 0 4.25 C 0 3.835938 0.335938 3.5 0.75 3.5 L 23.25 3.5 C 23.664062 3.5 24 3.835938 24 4.25 C 24 4.664062 23.664062 5 23.25 5 Z M 23.25 5 "
}), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "M 8.25 21.230469 C 7.835938 21.230469 7.5 20.894531 7.5 20.480469 L 7.5 10.25 C 7.5 9.007812 8.511719 8 9.75 8 C 10.988281 8 12 9.007812 12 10.25 L 12 16.25 C 12 16.664062 11.664062 17 11.25 17 C 10.835938 17 10.5 16.664062 10.5 16.25 L 10.5 10.25 C 10.5 9.835938 10.164062 9.5 9.75 9.5 C 9.335938 9.5 9 9.835938 9 10.25 L 9 20.480469 C 9 20.894531 8.664062 21.230469 8.25 21.230469 Z M 8.25 21.230469 "
}), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "M 11.25 19.5 C 10.835938 19.5 10.5 19.164062 10.5 18.75 L 10.5 16.25 C 10.5 15.007812 11.511719 14 12.75 14 C 13.988281 14 15 15.007812 15 16.25 L 15 17.25 C 15 17.664062 14.664062 18 14.25 18 C 13.835938 18 13.5 17.664062 13.5 17.25 L 13.5 16.25 C 13.5 15.835938 13.164062 15.5 12.75 15.5 C 12.335938 15.5 12 15.835938 12 16.25 L 12 18.75 C 12 19.164062 11.664062 19.5 11.25 19.5 Z M 11.25 19.5 "
}), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "M 14.25 20.5 C 13.835938 20.5 13.5 20.164062 13.5 19.75 L 13.5 17.25 C 13.5 16.007812 14.511719 15 15.75 15 C 16.988281 15 18 16.007812 18 17.25 L 18 19.25 C 18 19.664062 17.664062 20 17.25 20 C 16.835938 20 16.5 19.664062 16.5 19.25 L 16.5 17.25 C 16.5 16.835938 16.164062 16.5 15.75 16.5 C 15.335938 16.5 15 16.835938 15 17.25 L 15 19.75 C 15 20.164062 14.664062 20.5 14.25 20.5 Z M 14.25 20.5 "
}), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "M 20.25 24 C 19.835938 24 19.5 23.664062 19.5 23.25 L 19.5 19.25 C 19.5 18.835938 19.164062 18.5 18.75 18.5 C 18.335938 18.5 18 18.835938 18 19.25 L 18 20.75 C 18 21.164062 17.664062 21.5 17.25 21.5 C 16.835938 21.5 16.5 21.164062 16.5 20.75 L 16.5 19.25 C 16.5 18.007812 17.511719 17 18.75 17 C 19.988281 17 21 18.007812 21 19.25 L 21 23.25 C 21 23.664062 20.664062 24 20.25 24 Z M 20.25 24 "
}), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "M 5.359375 24 C 5.070312 24 4.792969 23.832031 4.671875 23.546875 C 3.964844 21.902344 3.324219 21.308594 2.859375 20.875 C 2.4375 20.484375 2 20.078125 2 19.25 C 2 18.957031 2.132812 17.5 4.75 17.5 C 6.589844 17.5 7.996094 18.878906 8.851562 20.03125 C 9.101562 20.367188 9.03125 20.835938 8.695312 21.082031 C 8.367188 21.328125 7.894531 21.257812 7.648438 20.925781 C 7.113281 20.207031 6.027344 19 4.75 19 C 3.703125 19 3.492188 19.289062 3.492188 19.289062 C 3.5 19.402344 3.5 19.425781 3.878906 19.777344 C 4.410156 20.269531 5.214844 21.015625 6.050781 22.953125 C 6.214844 23.332031 6.035156 23.773438 5.65625 23.9375 C 5.5625 23.980469 5.460938 24 5.359375 24 Z M 5.359375 24 "
})));
const community = Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  width: "24pt",
  height: "24pt",
  viewBox: "0 0 24 24",
  version: "1.1"
}, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("g", {
  id: "surface1"
}, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("path", {
  d: "M 19.957031 5.917969 C 20.808594 5.386719 21.375 4.449219 21.375 3.375 C 21.375 1.722656 20.027344 0.375 18.375 0.375 C 16.722656 0.375 15.375 1.722656 15.375 3.375 C 15.375 4.445312 15.941406 5.382812 16.785156 5.914062 C 15.824219 6.277344 15.007812 6.964844 14.480469 7.875 C 14.203125 7.6875 13.902344 7.539062 13.582031 7.417969 C 14.433594 6.886719 15 5.949219 15 4.875 C 15 3.222656 13.652344 1.875 12 1.875 C 10.347656 1.875 9 3.222656 9 4.875 C 9 5.949219 9.566406 6.886719 10.417969 7.417969 C 10.097656 7.539062 9.796875 7.6875 9.519531 7.875 C 8.992188 6.96875 8.175781 6.277344 7.214844 5.914062 C 8.058594 5.382812 8.625 4.445312 8.625 3.375 C 8.625 1.722656 7.277344 0.375 5.625 0.375 C 3.972656 0.375 2.625 1.722656 2.625 3.375 C 2.625 4.449219 3.191406 5.386719 4.042969 5.917969 C 2.339844 6.5625 1.125 8.203125 1.125 10.125 L 1.125 13.125 C 1.125 13.953125 1.796875 14.625 2.625 14.625 L 3 14.625 L 3 20.625 C 3 21.453125 3.671875 22.125 4.5 22.125 C 4.949219 22.125 5.351562 21.921875 5.625 21.605469 C 5.898438 21.921875 6.300781 22.125 6.75 22.125 C 7.578125 22.125 8.25 21.453125 8.25 20.625 L 8.25 15.917969 C 8.472656 16.046875 8.726562 16.125 9 16.125 L 9.375 16.125 L 9.375 22.125 C 9.375 22.953125 10.046875 23.625 10.875 23.625 C 11.324219 23.625 11.726562 23.421875 12 23.105469 C 12.273438 23.421875 12.675781 23.625 13.125 23.625 C 13.953125 23.625 14.625 22.953125 14.625 22.125 L 14.625 16.125 L 15 16.125 C 15.273438 16.125 15.527344 16.046875 15.75 15.917969 L 15.75 20.625 C 15.75 21.453125 16.421875 22.125 17.25 22.125 C 17.699219 22.125 18.101562 21.921875 18.375 21.605469 C 18.648438 21.921875 19.050781 22.125 19.5 22.125 C 20.328125 22.125 21 21.453125 21 20.625 L 21 14.625 L 21.375 14.625 C 22.203125 14.625 22.875 13.953125 22.875 13.125 L 22.875 10.125 C 22.875 8.203125 21.660156 6.5625 19.957031 5.917969 Z M 17.566406 6.472656 C 17.828125 6.414062 18.097656 6.375 18.375 6.375 C 18.65625 6.375 18.925781 6.40625 19.1875 6.464844 L 18.375 7.605469 Z M 17.886719 8.210938 L 17.394531 8.699219 L 16.671875 6.792969 C 16.722656 6.769531 16.769531 6.738281 16.816406 6.714844 Z M 18.863281 8.210938 L 19.929688 6.71875 C 19.980469 6.742188 20.03125 6.765625 20.078125 6.789062 L 19.355469 8.699219 Z M 16.125 3.375 C 16.125 2.132812 17.132812 1.125 18.375 1.125 C 19.617188 1.125 20.625 2.132812 20.625 3.375 C 20.625 4.617188 19.617188 5.625 18.375 5.625 C 17.132812 5.625 16.125 4.617188 16.125 3.375 Z M 12.367188 10.773438 L 12 11.324219 L 11.632812 10.773438 L 11.847656 7.882812 C 11.898438 7.878906 11.949219 7.875 12 7.875 C 12.050781 7.875 12.101562 7.878906 12.152344 7.882812 Z M 9.75 4.875 C 9.75 3.632812 10.757812 2.625 12 2.625 C 13.242188 2.625 14.25 3.632812 14.25 4.875 C 14.25 6.117188 13.242188 7.125 12 7.125 C 10.757812 7.125 9.75 6.117188 9.75 4.875 Z M 5.625 6.375 C 5.859375 6.375 6.085938 6.402344 6.308594 6.445312 C 6.191406 6.84375 5.925781 7.125 5.625 7.125 C 5.324219 7.125 5.058594 6.839844 4.941406 6.441406 C 5.164062 6.398438 5.390625 6.375 5.625 6.375 Z M 3.375 3.375 C 3.375 2.132812 4.382812 1.125 5.625 1.125 C 6.867188 1.125 7.875 2.132812 7.875 3.375 C 7.875 4.617188 6.867188 5.625 5.625 5.625 C 4.382812 5.625 3.375 4.617188 3.375 3.375 Z M 7.5 20.625 C 7.5 21.039062 7.164062 21.375 6.75 21.375 C 6.335938 21.375 6 21.039062 6 20.625 L 6 12.75 L 5.25 12.75 L 5.25 20.625 C 5.25 21.039062 4.914062 21.375 4.5 21.375 C 4.085938 21.375 3.75 21.039062 3.75 20.625 L 3.75 8.625 L 3 8.625 L 3 13.875 L 2.625 13.875 C 2.210938 13.875 1.875 13.539062 1.875 13.125 L 1.875 10.125 C 1.875 8.554688 2.847656 7.207031 4.222656 6.648438 C 4.433594 7.371094 4.980469 7.875 5.625 7.875 C 6.269531 7.875 6.8125 7.375 7.027344 6.65625 C 7.828125 6.980469 8.5 7.574219 8.917969 8.351562 C 8.046875 9.175781 7.5 10.335938 7.5 11.625 Z M 15 15.375 L 14.625 15.375 L 14.625 10.125 L 13.875 10.125 L 13.875 22.125 C 13.875 22.539062 13.539062 22.875 13.125 22.875 C 12.710938 22.875 12.375 22.539062 12.375 22.125 L 12.375 14.25 L 11.625 14.25 L 11.625 22.125 C 11.625 22.539062 11.289062 22.875 10.875 22.875 C 10.460938 22.875 10.125 22.539062 10.125 22.125 L 10.125 10.125 L 9.375 10.125 L 9.375 15.375 L 9 15.375 C 8.585938 15.375 8.25 15.039062 8.25 14.625 L 8.25 11.625 C 8.25 9.871094 9.460938 8.402344 11.085938 7.992188 L 10.867188 10.976562 L 12 12.675781 L 13.132812 10.976562 L 12.914062 7.992188 C 14.539062 8.402344 15.75 9.871094 15.75 11.625 L 15.75 14.625 C 15.75 15.039062 15.414062 15.375 15 15.375 Z M 22.125 13.125 C 22.125 13.539062 21.789062 13.875 21.375 13.875 L 21 13.875 L 21 9 L 20.25 9 L 20.25 20.625 C 20.25 21.039062 19.914062 21.375 19.5 21.375 C 19.085938 21.375 18.75 21.039062 18.75 20.625 L 18.75 12.75 L 18 12.75 L 18 20.625 C 18 21.039062 17.664062 21.375 17.25 21.375 C 16.835938 21.375 16.5 21.039062 16.5 20.625 L 16.5 11.625 C 16.5 10.335938 15.953125 9.175781 15.082031 8.351562 C 15.320312 7.90625 15.644531 7.519531 16.027344 7.210938 L 17.105469 10.050781 L 18.375 8.78125 L 19.644531 10.050781 L 20.722656 7.207031 C 21.578125 7.894531 22.125 8.945312 22.125 10.125 Z M 22.125 13.125 "
})));
/* harmony default export */ __webpack_exports__["default"] = ({
  clock,
  post,
  repeat,
  tabs,
  tab_item,
  community
});

/***/ }),

/***/ "./src/Blocks/iterator-items/index.js":
/*!********************************************!*\
  !*** ./src/Blocks/iterator-items/index.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/extends */ "./node_modules/@babel/runtime/helpers/extends.js");
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/blocks */ "@wordpress/blocks");
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _icons_icons__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../icons/icons */ "./src/Blocks/icons/icons.js");







/* harmony default export */ __webpack_exports__["default"] = (Object(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_2__["registerBlockType"])('cs-gutenberg-blocks/iterator-items', {
  apiVersion: 2,
  title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Block item', 'cumulus-block'),
  icon: _icons_icons__WEBPACK_IMPORTED_MODULE_6__["default"].tab_item,
  parent: ['cs-gutenberg-blocks/iterator'],
  description: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('A single item within a repeat block.', 'cumulus-block'),
  supports: {
    html: false
  },
  attributes: {
    displayitem: {
      type: 'boolean',
      default: true
    }
  },
  edit: props => {
    const {
      attributes,
      setAttributes,
      clientId
    } = props;
    const ALLOWED_BLOCKS = wp.blocks.getBlockTypes().filter(block => {
      return block.name.indexOf('cs-gutenberg-blocks/iterator') === -1;
    }).map(block => {
      return block.name;
    });
    return [Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__["InspectorControls"], {
      key: "inspector-controls-for-iterator-items"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(_wordpress_components__WEBPACK_IMPORTED_MODULE_5__["PanelBody"], {
      title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Wyświetl element', 'cumulus-block'),
      initialOpen: true
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(_wordpress_components__WEBPACK_IMPORTED_MODULE_5__["CheckboxControl"], {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Element aktywny', 'cumulus-block'),
      checked: attributes.displayitem,
      onChange: newValue => {
        setAttributes({
          displayitem: newValue
        });
      }
    }))), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])("div", _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
      key: clientId
    }, Object(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__["useBlockProps"])()), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__["InnerBlocks"], {
      allowedBlocks: ALLOWED_BLOCKS
    }))];
  },
  save: props => {
    const {
      attributes
    } = props;
    return attributes.displayitem && Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])("div", _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__["useBlockProps"].save(), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__["InnerBlocks"].Content, null));
  }
}));

/***/ }),

/***/ "./src/Blocks/iterator/index.js":
/*!**************************************!*\
  !*** ./src/Blocks/iterator/index.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/blocks */ "@wordpress/blocks");
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _icons_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../icons/icons */ "./src/Blocks/icons/icons.js");





/* harmony default export */ __webpack_exports__["default"] = (Object(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__["registerBlockType"])('cs-gutenberg-blocks/iterator', {
  apiVersion: 2,
  category: "cumulus",
  icon: _icons_icons__WEBPACK_IMPORTED_MODULE_4__["default"].repeat,
  textdomain: "cumulus-block",
  supports: {
    html: false
  },
  title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__["__"])('Iterator', 'cumulus-block'),
  description: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__["__"])('Blok iteratora', 'cumulus-block'),
  edit: () => {
    const ALLOWED_BLOCKS = ['cs-gutenberg-blocks/iterator-items'];
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", Object(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__["useBlockProps"])(), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("h2", {
      className: "wp-block-cs-gutenberg-blocks-iterator__heading"
    }, Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__["__"])('Blok iteratora', 'cumulus-block')), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__["InnerBlocks"], {
      allowedBlocks: ALLOWED_BLOCKS
    }));
  },
  save: () => {
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__["useBlockProps"].save(), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__["InnerBlocks"].Content, null));
  }
}));

/***/ }),

/***/ "./src/Blocks/member-team/index.js":
/*!*****************************************!*\
  !*** ./src/Blocks/member-team/index.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/extends */ "./node_modules/@babel/runtime/helpers/extends.js");
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/blocks */ "@wordpress/blocks");
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _icons_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../icons/icons */ "./src/Blocks/icons/icons.js");






const {
  RichText,
  InspectorControls
} = wp.blockEditor;
const {
  PanelBody,
  CheckboxControl,
  RadioControl
} = wp.components;
/* harmony default export */ __webpack_exports__["default"] = (Object(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_2__["registerBlockType"])('cs-gutenberg-blocks/member-team', {
  apiVersion: 2,
  title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Member Team', 'cumulus-block'),
  icon: _icons_icons__WEBPACK_IMPORTED_MODULE_5__["default"].community,
  category: "cumulus",
  description: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Dla jak najbardziej realistycznego wyglądu bloku, należy dodawać zdjęcia w proporcji 4/3.', 'cumulus-block'),
  supports: {
    html: false
  },
  attributes: {
    firstText: {
      type: "string",
      default: ""
    },
    secondText: {
      type: "string",
      default: ""
    },
    showSecondText: {
      type: "bool",
      default: true
    },
    postLayout: {
      type: "string",
      default: "main"
    }
  },
  edit: props => {
    const {
      attributes,
      setAttributes
    } = props;
    const ALLOWED_BLOCKS = ['core/image'];
    return [Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(InspectorControls, {
      key: "inspector-controls-for-members-team"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(PanelBody, {
      title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Ustawienia:'),
      initialOpen: true
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(RadioControl, {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Style'),
      selected: attributes.postLayout,
      options: [{
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Blue'),
        value: 'main'
      }, {
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Red'),
        value: 'second'
      }, {
        label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Tryb autora'),
        value: 'author'
      }],
      onChange: data => setAttributes({
        postLayout: data
      })
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(CheckboxControl, {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Pokaż drugi obszar tekstowy'),
      checked: attributes.showSecondText,
      onChange: newValue => {
        setAttributes({
          showSecondText: newValue
        });
      }
    }))), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])("div", _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, Object(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__["useBlockProps"])({
      className: `wp-block-cs-gutenberg-blocks-member-team--${attributes.postLayout}`
    }), {
      key: "edit-content-for-members-team"
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__["InnerBlocks"], {
      key: "main_innerblock",
      allowedBlocks: ALLOWED_BLOCKS
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(RichText, {
      key: "editable_1",
      tagName: "div",
      className: "wp-block-cs-gutenberg-blocks-member-team__text-1",
      placeholder: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])("Pierwszy obszar tekstowy"),
      multiline: true,
      value: attributes.firstText,
      onChange: text => setAttributes({
        firstText: text
      })
    }), attributes.showSecondText && Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(RichText, {
      key: "editable_2",
      tagName: "div",
      className: "wp-block-cs-gutenberg-blocks-member-team__text-2",
      placeholder: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])("Drugi obszar tekstowy"),
      multiline: true,
      value: attributes.secondText,
      onChange: text => setAttributes({
        secondText: text
      })
    }))];
  },
  save: ({
    attributes
  }) => {
    const i = 'wp-block-cs-gutenberg-blocks-member-team--' + attributes.postLayout;
    const blockProps = _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__["useBlockProps"].save({
      className: i
    });
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])("div", blockProps, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__["InnerBlocks"].Content, null), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(RichText.Content, {
      key: "editable_1",
      tagName: "div",
      className: "wp-block-cs-gutenberg-blocks-member-team__text-1",
      value: attributes.firstText
    }), attributes.showSecondText && Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(RichText.Content, {
      key: "editable_2",
      tagName: "div",
      className: "wp-block-cs-gutenberg-blocks-member-team__text-2",
      value: attributes.secondText
    }));
  }
}));

/***/ }),

/***/ "./src/Blocks/tabs-items/index.js":
/*!****************************************!*\
  !*** ./src/Blocks/tabs-items/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/blocks */ "@wordpress/blocks");
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _icons_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../icons/icons */ "./src/Blocks/icons/icons.js");





/* harmony default export */ __webpack_exports__["default"] = (Object(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__["registerBlockType"])('cs-gutenberg-blocks/tabs-item', {
  apiVersion: 2,
  title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__["__"])('Block tabs item', 'cumulus-block'),
  category: "cumulus",
  icon: _icons_icons__WEBPACK_IMPORTED_MODULE_4__["default"].tab_item,
  parent: ['cs-gutenberg-blocks/tabs'],
  description: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__["__"])('A single item within a repeat block.', 'cumulus-block'),
  supports: {
    html: false
  },
  attributes: {
    content: {
      type: 'string',
      default: ''
    },
    item_number: {
      type: 'string',
      default: ''
    }
  },
  edit: props => {
    const {
      attributes,
      setAttributes,
      clientId
    } = props;
    const ALLOWED_BLOCKS = wp.blocks.getBlockTypes().filter(block => {
      return block.name.indexOf('cs-gutenberg-blocks/tabs') === -1;
    }).map(block => {
      return block.name;
    });
    setAttributes({
      item_number: clientId
    });
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", Object(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__["useBlockProps"])(), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      className: "wp-block-cs-gutenberg-blocks-tabs__heading"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__["RichText"], {
      value: attributes.content,
      onChange: values => {
        setAttributes({
          content: values
        });
      },
      placeholder: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__["__"])('Heading...')
    })), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__["InnerBlocks"], {
      allowedBlocks: ALLOWED_BLOCKS
    }));
  },
  save: ({
    attributes
  }) => {
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__["useBlockProps"].save({
      id: `c_${attributes.item_number}`
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__["InnerBlocks"].Content, null));
  }
}));

/***/ }),

/***/ "./src/Blocks/tabs/index.js":
/*!**********************************!*\
  !*** ./src/Blocks/tabs/index.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/blocks */ "@wordpress/blocks");
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _icons_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../icons/icons */ "./src/Blocks/icons/icons.js");




const {
  useEffect
} = wp.element;
const {
  select,
  withSelect
} = wp.data;

/* harmony default export */ __webpack_exports__["default"] = (Object(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__["registerBlockType"])('cs-gutenberg-blocks/tabs', {
  apiVersion: 2,
  title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__["__"])('Blok tabs'),
  description: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__["__"])('Blok tabs', 'cumulus-block'),
  category: 'cumulus',
  icon: _icons_icons__WEBPACK_IMPORTED_MODULE_4__["default"].tabs,
  supports: {
    html: false
  },
  attributes: {
    content: {
      'type': 'string',
      'default': ''
    }
  },
  edit: withSelect((select, props) => {
    const {
      clientId
    } = props;
    return {
      c_block: select('core/block-editor').getBlock(clientId)
    };
  })(props => {
    const {
      attributes,
      c_block,
      setAttributes
    } = props;
    useEffect(() => {
      const t = c_block.innerBlocks.map(item => {
        return {
          content: item.attributes.content,
          id: item.attributes.item_number
        };
      });
      const T_stringfy = JSON.stringify(t);

      if (T_stringfy !== attributes.content) {
        setAttributes({
          content: T_stringfy
        });
      }
    });
    const ALLOWED_BLOCKS = ['cs-gutenberg-blocks/tabs-item'];
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", Object(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__["useBlockProps"])(), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("h2", {
      className: "wp-block-cs-gutenberg-blocks-tabs__heading"
    }, Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__["__"])('Blok tabs')), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__["InnerBlocks"], {
      allowedBlocks: ALLOWED_BLOCKS
    }));
  }),
  save: ({
    attributes
  }) => {
    const nav = JSON.parse(attributes.content).map((item, index) => {
      return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("li", {
        key: index
      }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("span", {
        className: index == 0 ? 'active tab-item' : 'tab-item',
        "data-c-item": item.id
      }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("span", {
        className: "cs-gutenberg-blocks-tabs__icon"
      }), item.content));
    });
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__["useBlockProps"].save(), nav && Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("ul", {
      className: "nav-tab"
    }, nav), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      className: "content-tab"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__["InnerBlocks"].Content, null)));
  }
}));

/***/ }),

/***/ "./src/editor.scss":
/*!*************************!*\
  !*** ./src/editor.scss ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/style.scss");
/* harmony import */ var _editor_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./editor.scss */ "./src/editor.scss");
/* harmony import */ var _Blocks_iterator_items_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Blocks/iterator-items/index */ "./src/Blocks/iterator-items/index.js");
/* harmony import */ var _Blocks_iterator_index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Blocks/iterator/index */ "./src/Blocks/iterator/index.js");
/* harmony import */ var _Blocks_display_posts_index__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Blocks/display-posts/index */ "./src/Blocks/display-posts/index.js");
/* harmony import */ var _Blocks_tabs_index__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Blocks/tabs/index */ "./src/Blocks/tabs/index.js");
/* harmony import */ var _Blocks_tabs_items_index__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Blocks/tabs-items/index */ "./src/Blocks/tabs-items/index.js");
/* harmony import */ var _Blocks_member_team_index__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./Blocks/member-team/index */ "./src/Blocks/member-team/index.js");
/**
 * Registers a new block provided a unique name and an object defining its behavior.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-registration/
 */









/***/ }),

/***/ "@wordpress/block-editor":
/*!*************************************!*\
  !*** external ["wp","blockEditor"] ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = window["wp"]["blockEditor"]; }());

/***/ }),

/***/ "@wordpress/blocks":
/*!********************************!*\
  !*** external ["wp","blocks"] ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = window["wp"]["blocks"]; }());

/***/ }),

/***/ "@wordpress/components":
/*!************************************!*\
  !*** external ["wp","components"] ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = window["wp"]["components"]; }());

/***/ }),

/***/ "@wordpress/element":
/*!*********************************!*\
  !*** external ["wp","element"] ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = window["wp"]["element"]; }());

/***/ }),

/***/ "@wordpress/i18n":
/*!******************************!*\
  !*** external ["wp","i18n"] ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = window["wp"]["i18n"]; }());

/***/ })

/******/ });
//# sourceMappingURL=index.js.map